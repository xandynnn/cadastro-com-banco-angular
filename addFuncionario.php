<?php require_once("header.php"); ?>

	<!-- Controller -->
	<script src="app/components/users/userController.js"></script>

	<div ng-controller="UsersController">

		<div class="col-md-12 col-sm-12 col-xs-12">
			<h1>Adicionar Funcionário GM5</h1>
		</div>

		<?php require_once("app/components/users/add/addUserView.php"); ?>

	</div>

<?php require_once("footer.php"); ?>