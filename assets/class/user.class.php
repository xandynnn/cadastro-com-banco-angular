<?php
require_once('dbconnect.class.php');
class user{

	private $id;
	private $nome;
	private $sobrenome;
	private $funcao;
	private $telefone;
	private $email;
	private $senha;
	private $celular;
	
	//Metodos Get e Set
	public function getId(){
		return $this->id;
	} 
	
	public function setId(){
		$this->id = NULL;
	}

	public function getNome(){
		return $this->nome;
	} 
	
	public function setNome($valor){
		$this->nome = $valor;
	}

	public function getSobrenome(){
		return $this->sobrenome;
	} 
	
	public function setSobrenome($valor){
		$this->sobrenome = $valor;
	}

	public function getFuncao(){
		return $this->funcao;
	} 
	
	public function setFuncao($valor){
		$this->funcao = $valor;
	}

	public function getTelefone(){
		return $this->telefone;
	} 
	
	public function setTelefone($valor){
		$this->telefone = $valor;
	}

	public function getEmail(){
		return $this->email;
	} 
	
	public function setEmail($valor){
		$this->email = $valor;
	}

	public function getSenha(){
		return $this->senha;
	} 
	
	public function setSenha($valor){
		$this->senha = $valor;
	}

	public function getCelular(){
		return $this->celular;
	} 
	
	public function setCelular($valor){
		$this->celular = $valor;
	}
			
	public static function listUser(){
		
		//Conecta ao banco
		$db = new dbconnect();
		
		//Faz a consulta
		$sql = "SELECT * FROM funcionarios"; 
		
		//Prepara a consulta
		$stmt = $db->prepare($sql);
		
		//Executa a consulta
		$stmt->execute();

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$json= json_encode($results);
		
		print($json);

	}

	public function addUser(){
		
		//Faz a conexão com o banco
		$db = new dbconnect();

		//Faz a consulta
		$queryStr = "INSERT into funcionarios ( id, nome, sobrenome, funcao, telefone, email, senha, celular ) values ( NULL, :nome , :sobrenome, :funcao, :telefone, :email, :senha, :celular )";

		//Prepara o statement
		$stmt = $db->prepare($queryStr);
		
		$stmt->bindParam(':nome',$this->nome);
		$stmt->bindParam(':sobrenome',$this->sobrenome);
		$stmt->bindParam(':funcao',$this->funcao);
		$stmt->bindParam(':telefone',$this->telefone);
		$stmt->bindParam(':email',$this->email);
		$stmt->bindParam(':senha',$this->senha);
		$stmt->bindParam(':celular',$this->celular);
		
		//Executa a query
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			return true;
		}else {
			return false;
		}
		
	}

	public function editUser(){
		
		//Conecta com o banco
		$db = new dbconnect();
		
		//Faz a consulta
		$queryStr = "UPDATE funcionarios SET
		nome = :nome,
		sobrenome = :sobrenome,
		funcao = :funcao,
		telefone = :telefone,
		email = :email,
		senha = :senha,
		celular = :celular
        WHERE id = :id";
		
		$stmt = $db->prepare($queryStr);
		
		$stmt->bindParam(':id',$this->id);
		$stmt->bindParam(':nome',$this->nome);
		$stmt->bindParam(':sobrenome',$this->sobrenome);
		$stmt->bindParam(':funcao',$this->funcao);
		$stmt->bindParam(':telefone',$this->telefone);
		$stmt->bindParam(':email',$this->email);
		$stmt->bindParam(':senha',$this->senha);
		$stmt->bindParam(':celular',$this->celular);

		$stmt->execute();

		if($stmt->rowCount() > 0) {
			return true;
		}else {
			return false;
		}
		
	}

	public function removeUser( $userID ){
		
		//Faz a conexao com o banco
		$db = new dbconnect();
		
		//Faz a consulta
		$queryStr = "DELETE FROM funcionarios WHERE id = :userId";
		
		//Prepara a consulta
		$stmt = $db->prepare($queryStr);
		
		//Prepara os parametros
		$stmt->bindParam(':userId',$userID);
		
		//Executa a consulta
		$stmt->execute();
	
		if( $stmt->rowCount() > 0 ) {
			return true;
		}else {
			return false;
		}
		
	}

}
?>