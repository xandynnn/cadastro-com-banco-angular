<?php
class dbconnect extends PDO{
	
	protected $server;
	protected $user;
	protected $pass;
	protected $dbName;
	protected $con;
	
	function __construct(){
	   	
		$this->setBd();
		
		try{
        	$this->con = parent::__construct("mysql:dbname=$this->dbName;host=$this->server",$this->user,$this->pass);
			
		}catch(PDOException $e){
			echo $e->getMessage();
			return false;
		}	
		return $this->con;
   }
	
	//Encerra a conexão
	public function dbClose(){
		unset($this->con);
	}

	private function setBd(){
		if ( $_SERVER['HTTP_HOST'] != 'localhost' ) {
			$this->server = 'localhost';
			$this->user ='root';
			$this->pass = '';
			$this->dbName = 'gm5';
		}else{
			$this->server = 'localhost';
			$this->user ='root';
			$this->pass = '';
			$this->dbName = 'gm5';
		}
	}

}
?>