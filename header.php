﻿<?php
	require_once("assets/class/system.class.php");
?>
<html>
	<!-- documentacao -->
	<!-- add, list, Update e delete : http://tech-blog.maddyzone.com/javascript/perform-addeditdeleteview-php-using-angular-js -->
	<!-- https://docs.angularjs.org/api/ng/filter/orderBy -->
	<!-- Muito boa documentação com exemplos -> http://jptacek.com/2013/10/angularjs-introduction/ -->
	<!-- Exemplos no github https://github.com/jptacek/AngularPeriodic -->
	<head>
		<title>GM5 - Cadastro de Funcionários<?php if ( isset($title)) { print " - $title"; } ?></title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/libs/bootstrap/bootstrap.css" />
		<link rel="stylesheet" href="assets/libs/alertify/css/alertify.core.css" />
		<link rel="stylesheet" href="assets/libs/alertify/css/alertify.default.css" />

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script src="assets/libs/alertify/js/alertify.js"></script>
		<script src="app/app.module.js"></script>
		<script src="app/angular-locale_pt-br.js"></script>
		
	</head>
	<body ng-app="gm5">

		<!-- Menu -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nm1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">GM5 Contatos</a>
				</div>

				<div class="collapse navbar-collapse" id="nm1">
					<ul class="nav navbar-nav">
						<li class="active">
							<a href="index.php">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Funcionários <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="addFuncionario.php">Adicionar</a></li>
								<li><a href="listFuncionario.php">Listar</a></li>
							</ul>
						</li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="sair.php">Sair</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<!-- Conteúdo -->
		<div class="container">
			<div class="row">