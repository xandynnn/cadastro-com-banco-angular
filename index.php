<?php require_once("header.php"); ?>

	<!-- User Controller ( Busca de telefone ) -->
	<script src="app/components/users/userController.js"></script>
	
	<!-- Título -->
	<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:40px;">
		<h1>Painel Administrativo</h1>
		<p>Escolha o que deseja fazer, utilizando o menu acima ou os painéis de administração abaixo.</p>
	</div>
	
	<!-- Administração -->
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Administrar Funcionários</h3>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<a href="addFuncionario.php" class="list-group-item">Adicionar Funcionário</a>
					<a href="listFuncionario.php" class="list-group-item">Listar Funcionários</a>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Busca de telefone -->
	<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12">
		<?php require_once("app/components/users/list/searchUserPhoneView.php"); ?>
	</div>

<?php require_once("footer.php"); ?>