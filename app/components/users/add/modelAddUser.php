<?php

	require_once("../../../../assets/class/user.class.php");

	$data = json_decode(file_get_contents("php://input"));

	$nome = $data->nome;
	$sobrenome = $data->sobrenome;
	$funcao	= $data->funcao;
	$telefone = $data->telefone;
	$email = $data->email;
	$senha = $data->senha;
	$celular = $data->celular;

	$user = new user();
	
	$user->setNome($nome);
	$user->setSobrenome($sobrenome);
	$user->setFuncao($funcao);
	$user->setTelefone($telefone);
	$user->setEmail($email);
	$user->setSenha($senha);
	$user->setCelular($celular);

	$user->addUser();

?>