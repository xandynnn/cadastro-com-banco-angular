<?php
	/*
		CADASTRA USUARIO
	*/
?>
	<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0;">
	<form name="funcionarioForm">
		
		<!-- Nome -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-required="true" ng-model="fieldNome" placeholder="Nome">
			</div>
		</div>
		
		<!-- Sobrenome -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-required="true" ng-model="fieldSobrenome" placeholder="Sobrenome">
			</div>
		</div>
		
		<!-- Funcao -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<select class="form-control" ng-model="fieldFuncao" ng-required="true" ng-options="funcao.nome for funcao in funcoes">
					<option value="">Selecione uma função</option>
				</select>
			</div>
		</div>

		<!-- Telefone -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" ng-pattern="/^\d{4}-\d{4}$/" name="telefone" class="form-control" ng-required="true" ng-model="fieldTelefone" placeholder="Telefone">
				<!-- Mensagens -->
				<div ng-show="funcionarioForm.telefone.$error.pattern">
					<span class="label label-warning">O campo Telefone deve ter o formato 3333-3333</span>
				</div>
			</div>
			
		</div>

		<!-- Email -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="email" class="form-control" ng-required="true" ng-model="fieldEmail" placeholder="E-mail">
			</div>
		</div>

		<!-- Senha -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="password" class="form-control" ng-required="true" ng-model="fieldSenha" placeholder="Senha">
			</div>
		</div>

		<!-- Celular -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" ng-pattern="/^\d{5}-\d{4}$/" class="form-control" name="celular" ng-required="true" ng-model="fieldCelular" placeholder="Celular">
				<!-- Mensagens -->
				<div ng-show="funcionarioForm.celular.$error.pattern">
					<span class="label label-warning">O campo Celular deve ter o formato 3333-3333</span>
				</div>
			</div>
		</div>

		<!-- Adicionar Funcionário -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<button ng-disabled="funcionarioForm.$invalid" ng-click="addUser()" class="btn btn-default">Adicionar Funcionário</button>
		</div>

	</form>
	</div>

	<!-- <div class="col-md-12 col-sm-12 col-xs-12" ng-if="funcionarioForm.$invalid">
		<div class="message">
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Ouh Droga!</strong> Não foi possível encontrar um funcionário com esse nome.
			</div>
		</div>
	</div>-->
	
	<div class="col-md-12 col-sm-12 col-xs-12" ng-if="message == 'success'">
		<div class="message">
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Muito Bem!</strong> O funcionário foi cadastrado no sistema</a>.
			</div>
		</div>
	</div>