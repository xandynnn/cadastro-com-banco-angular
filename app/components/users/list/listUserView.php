<?php
	/*
		VISUALIZA USUARIO
	*/
?>

	<!-- BUSCA -->
	<h3>Buscar Usuário</h3>

	<!-- Nome -->
	<div class="form-group">
		<input type="text" class="form-control" ng-model="fieldSearch" placeholder="Digite o nome do usuário">
	</div>
	
	<!-- Mensagens de Erro e Sucesso -->
	<div ng-if="message == 'success'">
		<div class="message">
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Muito Bem!</strong> O funcionário foi removido com Sucesso</a>.
			</div>
		</div>
	</div>

	<div ng-if="message == 'error'">
		<div class="message">
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Ouh Droga!</strong> Não foi possível remover o funcionário.
			</div>
		</div>
	</div>
	<!-- Fim Mensagens de Erro e Sucesso -->

	<div class="clearfix"></div>
	
	<div class="panel panel-default">

		<div class="panel-heading">Lista de Funcionários</div>
		
		<!-- Table -->
		<table class="table table-striped table-hover">
			<table class="table">
				<thead>
					<tr>
						<th> </th>
						<th><a href="" ng-click="predicate = 'id'; reverse=!reverse">Id</a></th>
						<th><a href="" ng-click="predicate = 'nome'; reverse=!reverse">Nome</a></th>
						<th><a href="" ng-click="predicate = 'sobrenome'; reverse=!reverse">Sobrenome</a></th>
						<th><a href="" ng-click="predicate = 'funcao'; reverse=!reverse">Função</a></th>
						<th><a href="" ng-click="predicate = 'telefone'; reverse=!reverse">Telefone</a></th>
						<th><a href="" ng-click="predicate = 'email'; reverse=!reverse">Email</a></th>
						<th><a href="" ng-click="predicate = 'celular'; reverse=!reverse">Celular</a></th>
						<th>Date</th>
						<th>Remove</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-class="{danger: funcionario.selecionado}" data-ng-class="{info: !($index%2), normal: ($index%2)}" ng-repeat="funcionario in funcionarios | orderBy: predicate:reverse | filter : { nome : fieldSearch } as results ">
						<td><input type="checkbox" ng-model="funcionario.selecionado"></td>
						<td>{{ funcionario.id }}</td>
						<td><a href="javascript:void(0);" ng-click="editUser(funcionario.id)">{{ funcionario.nome | uppercase }}</a></td>
						<td>{{ funcionario.sobrenome }}</td>
						<td>{{ funcionario.funcao }}</td>
						<td>{{ funcionario.telefone }}</td>
						<td>{{ funcionario.email }}</td>
						<td>{{ funcionario.celular }}</td>
						<td>{{ data | date:'dd/mm/yyyy' }}</td>
						<td><a href="javascript:void(0);" class="btn btn-danger btn-xs" ng-click="removeUser(funcionario.id,funcionario.nome+' '+funcionario.sobrenome)">Remover</a></td>
					</tr>
					<tr ng-if="results.length == 0">
						<td colspan="8">
							<div class="alert alert-dismissible alert-danger">
								<strong>Ouh Droga!</strong> Não foi possível encontrar um funcionário com esse nome.
							</div>
						</td>
					</tr>
				</tbody>
		</table>

	</div>