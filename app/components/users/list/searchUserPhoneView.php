<?php
	/*
		BUSCA O TELEFONE DO USUÁRIO
	*/
?>

	<div class="panel panel-info" ng-controller="UsersController">

		<div class="panel-heading">
			<h3 class="panel-title">Buscar Telefone </h3>
		</div>
		<div class="panel-body">

			<!-- Nome -->
			<div class="form-group">
				<input type="text" ng-keypress="count=count+1" ng-init="count=0" class="form-control" ng-model="fieldSearch" placeholder="Digite o nome do usuário">
			</div>
			
			<ul class="list-group" ng-repeat="funcionario in funcionarios | filter : { nome : fieldSearch } as results " ng-hide="$index>=1">
				<span ng-hide="fieldSearch=='' || count == 0" ng-open="fieldSearch!=''">
					<li class="list-group-item">
						{{ funcionario.nome }}
					</li>
					<li class="list-group-item">
						<span class="badge">Celular</span>
						<a href="tel:1413-3333">{{ funcionario.celular }}</a>
					</li>
					<li class="list-group-item">
						<span class="badge">Residencial</span>
						<a href="tel:1413-3333">{{ funcionario.telefone }}</a>
					</li>
				</span>
			</ul>

			<!-- Caso não tiver resultado -->
			<div ng-hide="count > 0" class="alert alert-dismissible alert-info">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Informação</strong> Digite o nome do funcionário na busca.
			</div>
			
			<!-- Caso não tiver resultado -->
			<div ng-if="results.length == 0" class="alert alert-dismissible alert-danger">
				<strong>Ouh Droga!</strong> Não foi possível encontrar um funcionário com esse nome.
			</div>

		</div>
	</div>