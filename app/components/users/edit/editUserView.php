<?php
	/*
		CADASTRA USUARIO
	*/
?>
	<div class="col-md-6 col-sm-6 col-xs-12" style="padding:0;">
		
		<!-- Nome -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-model="fieldNome" placeholder="Nome">
			</div>
		</div>
		
		<!-- Sobrenome -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-model="fieldSobrenome" placeholder="Sobrenome">
			</div>
		</div>
		
		<!-- Funcao -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-model="fieldFuncao" placeholder="Funcao">
			</div>
		</div>

		<!-- Telefone -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-model="fieldTelefone" placeholder="Telefone">
			</div>
		</div>

		<!-- Email -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="email" class="form-control" ng-model="fieldEmail" placeholder="E-mail">
			</div>
		</div>

		<!-- Senha -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="password" class="form-control" ng-model="fieldSenha" placeholder="Senha">
			</div>
		</div>

		<!-- Celular -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<input type="text" class="form-control" ng-model="fieldCelular" placeholder="Celular">
			</div>
		</div>

		<!-- Adicionar Funcionário -->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<button ng-click="addUser()" class="btn btn-default">Adicionar Funcionário</button>
		</div>

	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12" ng-if="message == 'success'">
		<div class="message">
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Muito Bem!</strong> O funcionário foi cadastrado no sistema</a>.
			</div>
		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12" ng-if="message == 'error'">
		<div class="message">
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Ouh Droga!</strong> Não foi possível encontrar um funcionário com esse nome.
			</div>
		</div>
	</div>