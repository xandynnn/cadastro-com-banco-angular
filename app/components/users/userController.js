gm5.controller('UsersController', ['$scope', '$http', function($scope, $http){

	//FUNÇÕES DO FUNCIONÁRIO
	$scope.funcoes = [
		{nome:"Estagiário", value:"Estagiário"},
		{nome:"Desenvolvedor Front-End", value:"Desenvolvedor Front-End"},
		{nome:"Desenvolvedor Back-End", value:"Desenvolvedor Back-End"},
		{nome:"Analista", value:"Analista"},
		{nome:"Gerente", value:"Gerente"},
		{nome:"Sócio", value:"Sócio"}
	];
	$scope.data = new Date();

	//RESET DO ALERTFY
	function resetAlertify() {
		alertify.set({
			labels : {
				ok     : "Ok",
				cancel : "Cancelar"
			},
			delay : 5000,
			buttonReverse : false,
			buttonFocus   : "ok"
		});
	}

	//CARREGAMENTO DA PÁGINA
	$scope.load = function(){
		$http.get("app/components/users/list/modelListUser.php").success(function(data){
			$scope.funcionarios = data;
		});
	}
	$scope.load();

	//ADICIONANDO FUNCIONARIO
	$scope.addUser = function ($index){
		$http.post('app/components/users/add/modelAddUser.php',
			{
				'nome'		: $scope.fieldNome,
				'sobrenome'	: $scope.fieldSobrenome,
				'funcao'	: $scope.fieldFuncao,
				'telefone'	: $scope.fieldTelefone,
				'email'		: $scope.fieldEmail,
				'senha'		: $scope.fieldSenha,
				'celular'	: $scope.fieldCelular
			}
		).success( function ( data, status, headers, config ) {
        	$scope.fieldNome="";
			$scope.fieldSobrenome="";
			$scope.fieldFuncao="";
			$scope.fieldTelefone="";
			$scope.fieldEmail="";
			$scope.fieldSenha="";
			$scope.fieldCelular="";
			$scope.message="success";
      }).error( function ( data, status, headers, config ) {
           $scope.message="error";
      });
	}

	//REMOVE USUÁRIO
	$scope.removeUser = function($uid,$nome){
		resetAlertify();
		alertify.confirm('Deseja realmente excluir '+$nome+' do sistema?', function (e) {
			if (e) {
				// REMOVE O USUÁRIO
				$http.post('app/components/users/remove/modelRemoveUser.php', 
					{
						'id'	: $uid
					}
				).success( function ( data, status, headers, config ) {
					$scope.load();
					$scope.message="success";
				}).error( function ( data, status, headers, config ) {
					$scope.message="error";
				});
			} else {
				//CASO PRECISE MANDAR UMA MENSAGEM
			}
		});
		return false;
	}

}]);