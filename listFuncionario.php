<?php require_once("header.php"); ?>

	<!-- Controller -->
	<script src="app/components/users/userController.js"></script>

	<div ng-controller="UsersController">

		<div class="col-md-12 col-sm-12 col-xs-12">
			<h1>Funcionários GM5</h1>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<?php require_once("app/components/users/list/listUserView.php"); ?>
		</div>

	</div>

<?php require_once("footer.php"); ?>